```
$ sudo apt install firewalld
$ sudo firewall-cmd --permanent --add-port=8081/tcp
$ sudo firewall-cmd --reload
```

```
# cat docker-compose.yml

version: "2.4"
services:
  nexus3:
    image: sonatype/nexus3
    volumes: 
    - ./nexus-data:/nexus-data
    ports:
    - 8081:8081
```

```
$ sudo docker-compose pull
$ sudo docker-compose up -d

$ sudo docker-compose ps
```
